/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
       
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#ifndef USERDIALOG_HPP
#define USERDIALOG_HPP

#include <QDialog>

namespace Ui {
class UserDialog;
}

class UserDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit UserDialog(QWidget *parent = 0);
    ~UserDialog();
    QString username() const;
    void setUsername(const QString &username);
    QString password() const;
    void setPassword(const QString &pasword);
    QString oldUsername() const;
    void setOldUsername(const QString &username);


private slots:
    void textChanged(const QString &text);

private:
    Ui::UserDialog *ui;
    QString m_oldUsername;
};

#endif // USERDIALOG_HPP
