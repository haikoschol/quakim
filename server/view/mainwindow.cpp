/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include <QListWidget>
#include <QPushButton>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QListWidget *MainWindow::userList()
{
    return ui->centralWidget->findChild<QListWidget*>("userList");
}

QPushButton *MainWindow::addUserButton()
{
    return ui->centralWidget->findChild<QPushButton*>("addUserButton");
}

QPushButton *MainWindow::editUserButton()
{
    return ui->centralWidget->findChild<QPushButton*>("editUserButton");
}

QPushButton *MainWindow::deleteUserButton()
{
    return ui->centralWidget->findChild<QPushButton*>("deleteUserButton");
}
