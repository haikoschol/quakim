/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
       
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include "userdialog.hpp"
#include "ui_userdialog.h"

#include <QDialogButtonBox>
#include <QPushButton>

UserDialog::UserDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserDialog)
{
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

    connect(ui->usernameEdit,
            SIGNAL(textChanged(QString)),
            this,
            SLOT(textChanged(QString)));

    connect(ui->passwordEdit,
            SIGNAL(textChanged(QString)),
            this,
            SLOT(textChanged(QString)));

    ui->usernameEdit->setFocus();
}

UserDialog::~UserDialog()
{
    delete ui;
}

QString UserDialog::username() const
{
    return ui->usernameEdit->text();
}

void UserDialog::setUsername(const QString &username)
{
    ui->usernameEdit->setText(username);
}

QString UserDialog::password() const
{
    return ui->passwordEdit->text();
}

void UserDialog::setPassword(const QString &password)
{
    ui->passwordEdit->setText(password);
}

QString UserDialog::oldUsername() const
{
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    return m_oldUsername;
}

void UserDialog::setOldUsername(const QString &username)
{
    m_oldUsername = username;
}

void UserDialog::textChanged(const QString &text)
{
    Q_UNUSED(text);
    QPushButton *ok = ui->buttonBox->button(QDialogButtonBox::Ok);
    if (!ok) return;

    QString username = ui->usernameEdit->text();
    QString password = ui->passwordEdit->text();
    ok->setEnabled(username.size() && password.size());
}
