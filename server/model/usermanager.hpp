/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
       
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#ifndef USERMANAGER_HPP
#define USERMANAGER_HPP

#include <common/model/user.hpp>
#include <server/model/database.hpp>

#include <QObject>

namespace quakim
{
namespace model
{

class UserManager : public QObject
{
    Q_OBJECT

public:
    enum STATUS {
        OK,
        DATABASE_WRITE_FAILED,
        PASSWORD_INCORRECT,
        USER_NOT_FOUND,
        USER_ALREADY_EXISTS,
        USERNAME_TRUNCATED,
        PASSWORD_TRUNCATED
    };

    /** Create a UserManager instance.

      \param db the database where users managed by this instance should
      be stored. The UserManager instance takes ownership of the passed
      object.

      \param parent [optional] the parent object
     */
    explicit UserManager(
            quakim::model::Database<quakim::model::User> *db,
            QObject *parent = 0);

    ~UserManager();

    STATUS authenticate(const QString &username, const QString &password) const;
    QVector<User> allUsers() const;
    User userByName(const QString &username) const;
    bool userExists(const QString &username) const;

    STATUS addUser(const QString &username,
                   const QString &password,
                   quint64 *id = NULL);

    STATUS deleteUser(const QString &username);
    STATUS deleteUser(quint64 id);
    STATUS changePassword(const QString &username, const QString &newPassword);
    
signals:

private:
    bool userExists(const char *username) const;

    mutable Database<User> *m_db;
};
}
}
#endif // USERMANAGER_HPP
