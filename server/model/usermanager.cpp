/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
       
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include "usermanager.hpp"

#include <common/util/convert.hpp>

#include <cstring>

using quakim::util::qstringToShortStr;
using quakim::util::strToQString;

namespace quakim {
namespace model {

UserManager::UserManager(Database<User> *db, QObject *parent) :
    QObject(parent),
    m_db(db)
{
}

UserManager::~UserManager()
{
    delete m_db;
}

UserManager::STATUS UserManager::authenticate(const QString &username,
                                              const QString &password) const
{
    char name[SHORT_STR];
    char pwd[SHORT_STR];

    if (!qstringToShortStr(username, name)) return USER_NOT_FOUND;
    if (!qstringToShortStr(password, pwd)) return PASSWORD_INCORRECT;

    QVector<User> users = m_db->fetchAll();
    foreach (User u, users) {
        if (std::strcmp(u.username, name) == 0) {
            if (std::strcmp(u.password, pwd) == 0) {
                return OK;
            } else {
                return PASSWORD_INCORRECT;
            }
        }
    }

    return USER_NOT_FOUND;
}

QVector<User> UserManager::allUsers() const
{
    return m_db->fetchAll();
}

User UserManager::userByName(const QString &username) const
{
    char name[SHORT_STR];

    User invalid;
    if (!qstringToShortStr(username, name)) return invalid;

    QVector<User> users = m_db->fetchAll();
    foreach (User u, users) {
        if (std::strcmp(u.username, name) == 0) {
            return u;
        }
    }

    return invalid;
}

bool UserManager::userExists(const QString &username) const
{
    char name[SHORT_STR];
    if (!qstringToShortStr(username, name)) return false;
    return userExists(name);
}

UserManager::STATUS UserManager::addUser(const QString &username,
                                         const QString &password,
                                         quint64 *id)
{
    char name[SHORT_STR];
    char pwd[SHORT_STR];

    if (!qstringToShortStr(username, name)) return USERNAME_TRUNCATED;
    if (!qstringToShortStr(password, pwd)) return PASSWORD_TRUNCATED;

    if (userExists(username)) return USER_ALREADY_EXISTS;

    User user;
    std::strcat(user.username, name);
    std::strcat(user.password, pwd);
    bool success = m_db->insert(user);

    if (id) {
        *id = user.id;
    }

    return success ? OK : DATABASE_WRITE_FAILED;
}

UserManager::STATUS UserManager::deleteUser(const QString &username)
{
    char name[SHORT_STR];
    if (!qstringToShortStr(username, name)) return USERNAME_TRUNCATED;

    QVector<User> users = allUsers();
    foreach (User u, users) {
        if (std::strcmp(u.username, name) == 0) {
            return deleteUser(u.id);
        }
    }

    return USER_NOT_FOUND;
}

UserManager::STATUS UserManager::deleteUser(quint64 id)
{
    return m_db->remove(id) ? OK : USER_NOT_FOUND;
}

UserManager::STATUS UserManager::changePassword(const QString &username,
                                                const QString &newPassword)
{
    char name[SHORT_STR];
    if (!qstringToShortStr(username, name)) return USERNAME_TRUNCATED;
    if (!userExists(name)) return USER_NOT_FOUND;

    char pwd[SHORT_STR];
    if (!qstringToShortStr(newPassword, pwd)) return PASSWORD_TRUNCATED;

    QVector<User> users = allUsers();
    foreach (User u, users) {
        if (std::strcmp(u.username, name) == 0) {
            bool success = m_db->remove(u.id);
            return success ? OK : DATABASE_WRITE_FAILED;
        }
    }

    return USER_NOT_FOUND;
}

// private

bool UserManager::userExists(const char *username) const
{
    QVector<User> users = allUsers();
    foreach (User u, users) {
        if (std::strcmp(u.username, username) == 0) return true;
    }
    return false;
}

}
}
