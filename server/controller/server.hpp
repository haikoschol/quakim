/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
       
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#ifndef SERVER_HPP
#define SERVER_HPP

#include <QObject>

class MainWindow;
class UserDialog;

namespace quakim
{

namespace model
{
class UserManager;
}

namespace controller
{

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);
    ~Server();
    void run();
    
private slots:
    void showAddUserDialog();
    void showEditUserDialog();
    void deleteUserDialog();
    void addUser();
    void deleteUser();
    void editUser();

private:
    void rebuildUserList();
    UserDialog *makeUserDialog();

    MainWindow *m_mainWindow;
    model::UserManager *m_userManager;
    UserDialog *m_userDialog;
};
}
}
#endif // SERVER_HPP
