/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
       
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include "server.hpp"

#include <view/mainwindow.hpp>
#include <view/userdialog.hpp>
#include <model/usermanager.hpp>
#include <common/util/convert.hpp>

#include <QDesktopServices>
#include <QDir>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QPushButton>

namespace quakim
{
namespace controller
{

using model::Database;
using model::User;
using model::UserManager;

Server::Server(QObject *parent) :
    QObject(parent),
    m_mainWindow(new MainWindow),
    m_userManager(NULL),
    m_userDialog(NULL)
{
    QDir docDir(QDesktopServices::storageLocation(
                QDesktopServices::DocumentsLocation));

    QString usersDbAbsPath = docDir.absoluteFilePath("users.db");

    Database<User> *db = new Database<User>(usersDbAbsPath);
    db->open();
    m_userManager = new UserManager(db, this);

    connect(m_mainWindow->addUserButton(),
            SIGNAL(clicked()),
            this,
            SLOT(showAddUserDialog()));

    connect(m_mainWindow->editUserButton(),
            SIGNAL(clicked()),
            this,
            SLOT(showEditUserDialog()));

    connect(m_mainWindow->deleteUserButton(),
            SIGNAL(clicked()),
            this,
            SLOT(deleteUser()));
}

Server::~Server()
{
    delete m_mainWindow;
}

void Server::run()
{
    rebuildUserList();
    m_mainWindow->show();
}

void Server::showAddUserDialog()
{
    UserDialog *dialog = makeUserDialog();
    connect(dialog, SIGNAL(accepted()), this, SLOT(addUser()));
    dialog->show();
}

void Server::showEditUserDialog()
{
    QListWidgetItem *currentItem = m_mainWindow->userList()->currentItem();
    if (!currentItem)
        return;

    QString username = currentItem->text();
    User user = m_userManager->userByName(username);

    UserDialog *dialog = makeUserDialog();
    connect(dialog, SIGNAL(accepted()), this, SLOT(editUser()));
    dialog->setUsername(username);
    dialog->setOldUsername(username);
    dialog->setPassword(util::strToQString(user.password));
    dialog->show();
}

void Server::deleteUserDialog()
{
    delete m_userDialog;
    m_userDialog = NULL;
}

void Server::addUser()
{
    if (!m_userDialog) return;

    QString username = m_userDialog->username();
    QString password = m_userDialog->password();
    UserManager::STATUS status = m_userManager->addUser(username, password);

    if (status == UserManager::OK) {
        rebuildUserList();
    } else {
        QMessageBox::warning(m_mainWindow, "Error", "Adding user failed.");
    }

    deleteUserDialog();
}

void Server::editUser()
{
    if (!m_userDialog) return;

    QString username = m_userDialog->oldUsername();
    QString password = m_userDialog->password();
    UserManager::STATUS status = m_userManager->deleteUser(username);
    if (status != UserManager::OK) {
        QMessageBox::warning(m_mainWindow, "Error", "Editing user failed.");
        deleteUserDialog();
        return;
    }

    status = m_userManager->addUser(m_userDialog->username(), password);

    if (status == UserManager::OK) {
        rebuildUserList();
    } else {
        QMessageBox::warning(m_mainWindow, "Error", "Editing user failed.");
    }

    deleteUserDialog();
}

void Server::deleteUser()
{
    QListWidgetItem *currentItem = m_mainWindow->userList()->currentItem();
    if (!currentItem) return;
    QString username = currentItem->text();
    QString msg("Really delete user ");
    msg += username + "?";

    QMessageBox::StandardButton answer = QMessageBox::question(
                m_mainWindow,
                "Delete User",
                msg,
                QMessageBox::Yes | QMessageBox::No);

    if (answer != QMessageBox::Yes) return;

    UserManager::STATUS status = m_userManager->deleteUser(username);
    if (status == UserManager::OK) {
        rebuildUserList();
    } else {
        QMessageBox::warning(m_mainWindow, "Error", "User deletion failed.");
    }
}

void Server::rebuildUserList()
{
    QListWidget *userList = m_mainWindow->userList();
    userList->clear();

    QVector<User> users = m_userManager->allUsers();

    if (users.empty()) {
        m_mainWindow->editUserButton()->setEnabled(false);
        m_mainWindow->deleteUserButton()->setEnabled(false);
        return;
    }

    m_mainWindow->editUserButton()->setEnabled(true);
    m_mainWindow->deleteUserButton()->setEnabled(true);

    foreach (const User &user, users) {
        userList->addItem(new QListWidgetItem(user.username));
    }
}

UserDialog *Server::makeUserDialog()
{
    if (m_userDialog) {
        delete m_userDialog;
    }

    m_userDialog = new UserDialog(m_mainWindow);
    m_userDialog->setModal(true);
    connect(m_userDialog, SIGNAL(rejected()), this, SLOT(deleteUserDialog()));
    return m_userDialog;
}
}
}
