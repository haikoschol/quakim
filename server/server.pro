#-------------------------------------------------
#
# Project created by QtCreator 2011-12-22T18:50:21
#
#-------------------------------------------------

QT       += core gui

TARGET = server
TEMPLATE = app

INCLUDEPATH += ..

SOURCES += main.cpp\
        view/mainwindow.cpp \
    controller/server.cpp \
    model/usermanager.cpp \
    ../common/util/convert.cpp \
    view/userdialog.cpp

HEADERS  += view/mainwindow.hpp \
    model/database.hpp \
    ../common/model/user.hpp \
    ../common/defs.hpp \
    controller/server.hpp \
    model/usermanager.hpp \
    ../common/util/convert.hpp \
    view/userdialog.hpp

FORMS    += view/mainwindow.ui \
    view/userdialog.ui
