#-------------------------------------------------
#
# Project created by QtCreator 2012-02-01T12:03:38
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_usermanagertest
CONFIG   += testcase
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += $$PWD/../..
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += ../../server/model/usermanager.hpp \
            ../../common/util/convert.hpp

SOURCES += tst_usermanagertest.cpp \
            ../../server/model/usermanager.cpp \
            ../../common/util/convert.cpp
