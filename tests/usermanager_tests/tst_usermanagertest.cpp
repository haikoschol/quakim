/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
       
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#include <server/model/usermanager.hpp>

#include <QtCore/QString>
#include <QtTest/QtTest>
#include <QBuffer>
#include <QSharedPointer>

#include <cstring>

using namespace quakim::model;

class UserManagerTest : public QObject
{
    Q_OBJECT
    
public:
    UserManagerTest();
    
private:
    typedef QSharedPointer<UserManager> UserManagerPtr;
    UserManagerPtr makeUserManager();

private Q_SLOTS:
    void authenticate_with_existing_user_and_correct_password_succeeds();
    void authenticate_with_existing_user_and_incorrect_password_fails();
    void authenticate_with_nonexistent_user_fails();
    void addUser_with_nonexistent_user_succeeds();
    void addUser_with_existing_user_fails();
    void deleteUser_with_existing_user_succeeds();
    void deleteUser_with_nonexistent_user_fails();
    void changePassword_with_existing_user_succeeds();
    void changePassword_with_nonexistent_user_fails();
};

UserManagerTest::UserManagerTest() :
    QObject(NULL)
{
}

UserManagerTest::UserManagerPtr UserManagerTest::makeUserManager()
{
    QBuffer *store = new QBuffer;
    Database<User> *db = new Database<User>(store);
    User bart;
    std::strcat(bart.username, "bartsimpson");
    std::strcat(bart.password, "ididntdoit");
    db->open();
    db->insert(bart);
    return UserManagerPtr(new UserManager(db));
}

void UserManagerTest::authenticate_with_existing_user_and_correct_password_succeeds()
{
    UserManagerPtr userManager = makeUserManager();
    UserManager::STATUS status = userManager->authenticate("bartsimpson",
                                                             "ididntdoit");

    QCOMPARE(status, UserManager::OK);
}

void UserManagerTest::authenticate_with_existing_user_and_incorrect_password_fails()
{
    UserManagerPtr userManager = makeUserManager();
    UserManager::STATUS status = userManager->authenticate("bartsimpson",
                                                             "wrongpassword");

    QCOMPARE(status, UserManager::PASSWORD_INCORRECT);
}

void UserManagerTest::authenticate_with_nonexistent_user_fails()
{
    UserManagerPtr userManager = makeUserManager();
    UserManager::STATUS status = userManager->authenticate("lisasimpson",
                                                             "wrongpassword");

    QCOMPARE(status, UserManager::USER_NOT_FOUND);
}

void UserManagerTest::addUser_with_nonexistent_user_succeeds()
{
    UserManagerPtr userManager = makeUserManager();
    int userCountBefore = userManager->allUsers().size();
    UserManager::STATUS status = userManager->addUser("lisasimpson", "sax");
    QCOMPARE(status, UserManager::OK);
    QCOMPARE(userManager->allUsers().size(), userCountBefore + 1);
}

void UserManagerTest::addUser_with_existing_user_fails()
{
    UserManagerPtr userManager = makeUserManager();
    int userCountBefore = userManager->allUsers().size();
    UserManager::STATUS status = userManager->addUser("bartsimpson", "foo");
    QCOMPARE(status, UserManager::USER_ALREADY_EXISTS);
    QCOMPARE(userManager->allUsers().size(), userCountBefore);
}

void UserManagerTest::deleteUser_with_existing_user_succeeds()
{
    UserManagerPtr userManager = makeUserManager();
    int userCountBefore = userManager->allUsers().size();
    UserManager::STATUS status = userManager->deleteUser("bartsimpson");
    QCOMPARE(status, UserManager::OK);
    QCOMPARE(userManager->allUsers().size(), userCountBefore - 1);
}

void UserManagerTest::deleteUser_with_nonexistent_user_fails()
{
    UserManagerPtr userManager = makeUserManager();
    int userCountBefore = userManager->allUsers().size();
    UserManager::STATUS status = userManager->deleteUser("lisasimpson");
    QCOMPARE(status, UserManager::USER_NOT_FOUND);
    QCOMPARE(userManager->allUsers().size(), userCountBefore);
}

void UserManagerTest::changePassword_with_existing_user_succeeds()
{
    UserManagerPtr userManager = makeUserManager();
    UserManager::STATUS status = userManager->changePassword("bartsimpson",
                                                             "skateordie");
    QCOMPARE(status, UserManager::OK);
    QVERIFY(userManager->authenticate("bartsimpson", "skateordie"));
}

void UserManagerTest::changePassword_with_nonexistent_user_fails()
{
    UserManagerPtr userManager = makeUserManager();
    UserManager::STATUS status = userManager->changePassword("lisasimpson",
                                                             "jazzman");
    QCOMPARE(status, UserManager::USER_NOT_FOUND);
}

QTEST_APPLESS_MAIN(UserManagerTest)

#include "tst_usermanagertest.moc"
