#-------------------------------------------------
#
# Project created by QtCreator 2011-12-22T18:55:35
#
#-------------------------------------------------

QT       += testlib

QT       += gui

TARGET = tst_databasetest
CONFIG   += testcase
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += $$PWD/../..

SOURCES += tst_databasetest.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
