/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include <server/model/database.hpp>

#include <QBuffer>
#include <QString>
#include <QtTest/QtTest>

#include <cstring>

using namespace quakim::model;

struct Person {
    quint64 id;
    char firstName[255];
    char lastName[255];

    Person() : id(0)
    {
        std::memset(firstName, 0, sizeof firstName);
        std::memset(lastName, 0, sizeof lastName);
    }
};

class DatabaseTest : public QObject
{
    Q_OBJECT
    
public:
    DatabaseTest();
    
    QVector<Person> m_people;

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void constructor();
    void empty_after_construction();
    void insert_record();
    void reading_all_records_preserves_insertion_order();
    void retrieve_record_by_id();
    void remove_record();
    void remove_nonexistent_record_returns_false();
    void update_record();
    void update_nonexistent_record_fails();
};

DatabaseTest::DatabaseTest() :
    QObject(NULL)
{
}

void DatabaseTest::initTestCase()
{
    Person p1;
    std::strcat(p1.firstName, "Lisa");
    std::strcat(p1.lastName, "Simpson");
    m_people.append(p1);

    Person p2;
    std::strcat(p2.firstName, "Jimi");
    std::strcat(p2.lastName, "Hendrix");
    m_people.append(p2);

    Person p3;
    std::strcat(p3.firstName, "Albert");
    std::strcat(p3.lastName, "Einstein");
    m_people.append(p3);
}

void DatabaseTest::cleanupTestCase()
{
    m_people.clear();
}

void DatabaseTest::constructor()
{
    Database<Person> *db = new Database<Person>("foo");
    QVERIFY(db);
    delete db;
}

void DatabaseTest::empty_after_construction()
{
    QBuffer *buffer = new QBuffer;
    Database<Person> db(buffer);
    QVERIFY(db.open());
    QVector<Person> allRecords = db.fetchAll();
    QCOMPARE(allRecords.size(), 0);
}

void DatabaseTest::insert_record()
{
    QBuffer *buffer = new QBuffer;
    Database<Person> db(buffer);
    QVERIFY(db.open());

    Person p = m_people.at(0);
    qint64 sizeBefore = db.size();
    QVERIFY(db.insert(p));
    QVERIFY(db.size() > sizeBefore);
}

void DatabaseTest::reading_all_records_preserves_insertion_order()
{
    QBuffer *buffer = new QBuffer;
    Database<Person> db(buffer);
    QVERIFY(db.open());

    for (int i = 0; i < m_people.size(); i++) {
        Person p = m_people.at(i);
        QVERIFY(db.insert(p));
    }

    QVector<Person> allRecords = db.fetchAll();
    QCOMPARE(allRecords.size(), m_people.size());

    for (int i = 0; i < allRecords.size(); i++) {
        QVERIFY(std::strcmp(allRecords[i].firstName,
                            m_people.at(i).firstName) == 0);

        QVERIFY(std::strcmp(allRecords[i].lastName,
                            m_people.at(i).lastName) == 0);
    }
}

void DatabaseTest::retrieve_record_by_id()
{
    QBuffer *buffer = new QBuffer;
    Database<Person> db(buffer);
    QVERIFY(db.open());
    Person jimi;

    for (int i = 0; i < m_people.size(); i++) {
        Person p = m_people.at(i);
        QVERIFY(db.insert(p));
        if (i == 1) jimi = p;
    }

    Person retrieved;
    QVERIFY(db.fetch(jimi.id, retrieved));
    QCOMPARE(retrieved.id, jimi.id);
    QCOMPARE(retrieved.firstName, jimi.firstName);
    QCOMPARE(retrieved.lastName, jimi.lastName);
}

void DatabaseTest::remove_record()
{
    QBuffer *buffer = new QBuffer;
    Database<Person> db(buffer);
    QVERIFY(db.open());
    Person jimi;

    foreach (Person p, m_people) {
        QVERIFY(db.insert(p));
        if (QString(p.firstName) == "Jimi") jimi = p;
    }

    QVERIFY(db.remove(jimi.id));
    Person retrieved;
    QVERIFY(!db.fetch(jimi.id, retrieved));
}

void DatabaseTest::remove_nonexistent_record_returns_false()
{
    QBuffer *buffer = new QBuffer;
    Database<Person> db(buffer);
    QVERIFY(db.open());
    quint64 lastId = 0;

    foreach (Person p, m_people) {
        QVERIFY(db.insert(p));
        lastId = p.id;
    }

    qint64 before = db.size();
    QVERIFY(!db.remove(lastId + 1));
    QCOMPARE(db.size(), before);
}

void DatabaseTest::update_record()
{
    QBuffer *buffer = new QBuffer;
    Database<Person> db(buffer);
    QVERIFY(db.open());
    Person jimi;

    foreach (Person p, m_people) {
        QVERIFY(db.insert(p));
        if (QString(p.firstName) == "Jimi") jimi = p;
    }

    std::strcat(jimi.lastName, " the Awesome");
    QVERIFY(db.update(jimi));
    Person retrieved;
    QVERIFY(db.fetch(jimi.id, retrieved));
    QCOMPARE(jimi.firstName, retrieved.firstName);
    QCOMPARE(jimi.lastName, retrieved.lastName);
}

void DatabaseTest::update_nonexistent_record_fails()
{
    QBuffer *buffer = new QBuffer;
    Database<Person> db(buffer);
    QVERIFY(db.open());

    foreach (Person p, m_people) {
        QVERIFY(db.insert(p));
    }

    Person bart;
    std::strcat(bart.firstName, "Bart");
    std::strcat(bart.lastName, "Simpson");
    QVERIFY(!db.update(bart));
}

QTEST_APPLESS_MAIN(DatabaseTest)

#include "tst_databasetest.moc"
