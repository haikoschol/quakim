/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
       
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#ifndef CONVERT_HPP
#define CONVERT_HPP

#include <common/defs.hpp>

#include <QString>

namespace quakim {
namespace util {

/** Convert a QString to a char array with max. length SHORT_STR.
  \returns true, if the whole input string could be converted.
  */
bool qstringToShortStr(const QString &in, char *out);

/** Convert a QString to a char array with max. length MEDIUM_STR.
  \returns true, if the whole input string could be converted.
  */
bool qstringToMediumStr(const QString &in, char *out);

/** Convert a QString to a char array with max. length LONG_STR.
  \returns true, if the whole input string could be converted.
  */
bool qstringToLongStr(const QString &in, char *out);

/** Convert a char array to a QString.
  */
QString strToQString(const char *s);

}
}

#endif // CONVERT_HPP
