/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
       
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <common/util/convert.hpp>

#include <cstring>

namespace quakim {
namespace util {

bool qstringToStr(const QString &in, char *out, size_t maxLen)
{
    if (!out || maxLen < 1) return false;
    QByteArray utf8 = in.toUtf8();
    std::strncpy(out, utf8.data(), maxLen - 1);
    out[maxLen] = '\0';
    return static_cast<size_t>(utf8.size()) < maxLen;
}

bool qstringToShortStr(const QString &in, char *out)
{
    return qstringToStr(in, out, SHORT_STR_MAX_LEN);
}

bool qstringToMediumStr(const QString &in, char *out)
{
    return qstringToStr(in, out, MEDIUM_STR_MAX_LEN);
}

bool qstringToLongStr(const QString &in, char *out)
{
    return qstringToStr(in, out, LONG_STR_MAX_LEN);
}

QString strToQString(const char *s)
{
    return QString::fromUtf8(s);
}

}
}

