/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#ifndef DEFS_HPP
#define DEFS_HPP

namespace quakim
{
    static const int SHORT_STR = 16;
    static const int SHORT_STR_MAX_LEN = SHORT_STR - 1;
    static const int MEDIUM_STR = 64;
    static const int MEDIUM_STR_MAX_LEN = MEDIUM_STR - 1;
    static const int LONG_STR = 256;
    static const int LONG_STR_MAX_LEN = LONG_STR - 1;
}
#endif // DEFS_HPP
