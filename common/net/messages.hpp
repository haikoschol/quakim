/*
   Copyright 2011-2012 Haiko Schol <hs@haikoschol.com>
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
       
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


#ifndef MESSAGES_HPP
#define MESSAGES_HPP

#include <common/defs.hpp>

#include <cstring>

namespace quakim
{
namespace net
{
namespace messages
{

const quint32 ERROR = 0;
const quint32 AUTHENTICATE = 1;
const quint32 AUTHENTICATED = 2;
const quint32 LIST_USERS = 3;
const quint32 ADD_USER = 4;
const quint32 DELETE_USER = 5;
const quint32 UPDATE_USER = 6;

struct Header
{
    quint32 messageId;
    quint32 recordCount;

    Header() : messageId(0), recordCount(0) {}
};

struct Error
{
    const char description[LONG_STR];

    Error() { ::memset(description, 0, LONG_STR); }
};

struct Login
{
    const char username[MEDIUM_STR];
    const char password[MEDIUM_STR];

    Login()
    {
        ::memset(username, 0, MEDIUM_STR);
        ::memset(password, 0, MEDIUM_STR);
    }
};

}
}
}
#endif // MESSAGES_HPP
