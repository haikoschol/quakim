#-------------------------------------------------
#
# Project created by QtCreator 2011-12-22T18:51:42
#
#-------------------------------------------------

QT       += core gui

TARGET = client
TEMPLATE = app

INCLUDEPATH += ..

SOURCES += main.cpp\
        view/mainwindow.cpp

HEADERS  += view/mainwindow.hpp

FORMS    += view/mainwindow.ui
